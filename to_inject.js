var overlay_css = '<style type="text/css">#overlay{position:fixed;top:0px;left:0px;width:100%;height:100%;background:rgba(255,255,255,0.0);z-index:9999;pointer-events:none;}</style>',
    overlay_html = '<div id="overlay"></div>',
    sanny_url = 'http://i.imgur.com/DQaRNZL.png',
    marrissa_url = 'http://i.imgur.com/IOyOyU6.png',
    alex_url = 'http://i.imgur.com/df5qpwK.png',
    matt_url = 'http://i.imgur.com/263rKqJ.png',
    olaf_url = 'http://i.imgur.com/zxDKA01.png',
    elsa_url = 'http://i.imgur.com/QNVYM5j.png',
    anna_url = 'http://i.imgur.com/3Bu09Dm.png',
    sven_url = 'http://i.imgur.com/qcknwy8.png',
    trolls_url = 'http://i.imgur.com/DspeTAO.png';

function load_jquery() {
    var startingTime = new Date().getTime();
    // Load the script
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existance
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 10);
        }
    };

    // Start polling...
    checkReady(function($) {
        $(function() {
            var endingTime = new Date().getTime();
            var tookTime = endingTime - startingTime;
            console.log("jQuery is loaded, after " + tookTime + " milliseconds!");

            do_stuff();
        });
    });
}




function do_stuff() {
    $('head').append(overlay_css);
    $('body').append(overlay_html);

    // setup_some_text("frozenify", "FROZENIFY");
    // flash_some_text("frozenify", 200);

    // setup_some_image("olaf", "http://aimg.disneystore.com/content/ds/skyway/2013/flyout/flyout_frozen-olaf_20130916.png");
    // move_some_image('olaf', 2000);

    // setup_some_image("olaf", "http://aimg.disneystore.com/content/ds/skyway/2013/flyout/flyout_frozen-olaf_20130916.png");
    // slideup_some_image_from_bottom('olaf');

    var people = [
        ["sanny", sanny_url],
        ["marrissa", marrissa_url],
        ["matt", matt_url],
        ["alex", alex_url],
        ["olaf", olaf_url],
        ["elsa", elsa_url],
        ["anna", anna_url],
        ["sven", sven_url],
        ["trolls", trolls_url]
    ];
    for (var i = 0; i < people.length; i++) {
        setup_some_image(people[i][0], people[i][1])
        move_some_image(people[i][0], 2000);
    }
}





function flash_some_text(id, interval) {
    var tid = setInterval(function() {
        toggle_some_text(id);
    }, interval);
}

function setup_some_text(id, visible_text) {
    var text = $('#' + id);
    if (text.length) {
        text.hide();
        text.html(visible_text);
    } else {
        var some_text_html = '<h1 id="' + id + '" style="display: none;">' + visible_text + '</h1>';
        $('#overlay').append(some_text_html);
    }
}

function toggle_some_text(id) {
    var text = $('#' + id);
    if (text.is(":visible")) {
        text.hide();
    } else {
        show_some_text(id);
    }
}

function show_some_text(id) {
    var text = $('#' + id);
    var minTop = 0,
        minLeft = -200,
        maxTop = minTop + $("#overlay").outerHeight(),
        maxLeft = minLeft + $("#overlay").outerWidth();

    console.log("Top-Left: (" + minLeft.toString() + "," + minTop.toString() + ")");
    console.log("Bottom-Right: (" + maxLeft.toString() + "," + maxTop.toString() + ")");

    var top = Math.ceil(Math.random() * (maxTop-minTop)) + minTop,
        left = Math.ceil(Math.random() * (maxLeft-minLeft)) + minLeft,
        size = Math.ceil(Math.random() * (400-200)) + 200,
        weight = Math.ceil(Math.random() * (900-600)) + 600,
        colors = ["aqua", "black", "blue", "fuchsia", "gray", "green", "lime", "maroon", "navy", "olive", "orange", "purple", "red", "silver", "teal", "white", "yellow"],
        color = colors[Math.floor(Math.random() * colors.length)];

    console.log("Text-Location: (" + left.toString() + "," + top.toString() + ")");
    console.log("Text-Size: " + size.toString() + "%");
    console.log("Text-Color: " + color);

    text.css({ "margin-top": top + "px", "margin-left": left + "px", "font-size": size + "%", "font-weight": weight }); // , "color": color
    text.show();
};





function flash_some_image(id, interval) {
    var tid = setInterval(function() {
        toggle_some_image(id);
    }, interval);
}

function move_some_image(id, interval) {
    var tid = setInterval(function() {
        show_some_image(id);
    }, interval);
}

function setup_some_image(id, url) {
    var image = $('#' + id);
    if (image.length) {
        image.hide();
        image.attr("src", url);
    } else {
        var img_html = '<img id="' + id + '" src="' + url + '" alt="' + id + '" style="display: none;" class="elsa">'
        $('#overlay').append(img_html);
    }
}

function toggle_some_image(id) {
    var image = $('#' + id);
    if (image.is(":visible")) {
        image.hide();
    } else {
        show_some_image(id);
    }
}

function show_some_image(id) {
    var image = $('#' + id);
    var minTop = 0,
        minLeft = 0,
        maxTop = minTop + $("#overlay").outerHeight() - image.height(),
        maxLeft = minLeft + $("#overlay").outerWidth() - image.width();

    console.log("Top-Left: (" + minLeft.toString() + "," + minTop.toString() + ")");
    console.log("Bottom-Right: (" + maxLeft.toString() + "," + maxTop.toString() + ")");

    var top = Math.ceil(Math.random() * (maxTop-minTop)) + minTop,
        left = Math.ceil(Math.random() * (maxLeft-minLeft)) + minLeft;

    console.log("Image-Location: (" + left.toString() + "," + top.toString() + ")");

    image.css({ "top": top + "px", "left": left + "px", "position": "absolute" });
    image.show();
}

function slideup_some_image_from_bottom(id) {
    var image = $('#' + id);
    var maxTop = $("#overlay").outerHeight() - image.height(),
        maxLeft = $("#overlay").outerWidth() - image.width();

    var top = maxTop,
        left = Math.ceil(Math.random() * maxLeft);

    console.log("Image-Location: (" + left.toString() + "," + top.toString() + ")");

    image.css({ "margin-top": top + "px", "margin-left": left + "px" });
    image.slideUp(3000);
}



(function() {
    load_jquery();
})();
